# Django-Payment-App

##### Running on local

* Via Docker

  ```
  docker-compose up

  ```
* Running on local

  * Installation

    * install python >=3.6
    * install all dependency

      ```

      pip3 install -r requirements.txt
      ```
  * Database setup

    * current app is using sql database so create a db with name `payment_app` or you can update database name in `settings.py`
  * run migration using below commands

    ```
    python3 manage.py makemigrations paymentApp
    python3 manage.py migrate
    ```
  * now we can start server with these command

    ```
    python3 manage.py runserver

    ```

##### PlayAround the app

* `resource/payment_app.postman_collection.json` is a postman collection which can be used for play around the app
* APIs exposed:
  * `POST add-balance` : to be used for creating a new user or adding balance in a user account
  * `GET check-balance`: get account balance of a user
  * `POST make-payment`: make payment between two user for a given amount
  * `POST request-payment`: request payment from a user
  * `GET owns-money-from`: get list of users with amount whom current user owns money from
  * `GET owns-money-to`: get list of users with amount whom current user owns money from
