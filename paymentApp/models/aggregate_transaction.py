from django.db import models

class AggregateBalance(models.Model):
    id = models.CharField(max_length=36, primary_key=True)
    from_user = models.CharField(max_length=36)
    to_user = models.CharField(max_length=36)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    amount = models.FloatField()
    class Meta:
        db_table = "aggregate_balance"