from django.db import models

class Transaction(models.Model):
    id = models.CharField(max_length=36, primary_key=True)
    from_user = models.CharField(max_length=36)
    to_user = models.CharField(max_length=36)
    amount = models.FloatField(default=0)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    status = models.CharField(max_length=10, default="STARTED")

    class Meta:
        db_table = 'transaction_history'