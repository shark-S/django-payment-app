from django.db import models

class Wallet(models.Model):
    user_id = models.CharField(max_length=36, primary_key=True)  #user_id as uuid
    balance = models.FloatField()

    class Meta:
        db_table = 'wallet'