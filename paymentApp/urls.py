"""paymentApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('check-balance/<str:user_id>',views.check_balance, name='check_balance'),
    path('add-balance', views.add_balance, name='add_balance'),
    path('make-payment', views.make_payment, name = 'make_payment'),
    path('request-payment', views.request_payment, name = 'request_payment'),
    path('owns-money-from/<str:user_id>', views.owns_money_from, name = 'owns_money_from'),
    path('owns-money-to/<str:user_id>', views.owns_money_to, name = 'owns_money_to')
]
