from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotAllowed
import logging
from . import wallet_service
from django.views.decorators.csrf import csrf_exempt
import json
from django.db import transaction

logger = logging.getLogger(__name__)

@csrf_exempt 
def check_balance(request, user_id):
    logging.info("check balance request for user_id %s", user_id)

    if user_id is None:
        logger.error("Invalid user_id")
        return HttpResponseBadRequest("Invalid user_id")
    else:
        try:
            balance = wallet_service.get_balance(user_id)
            return HttpResponse(balance)
        except Exception as exception:
            logger.error("Error while fetching balance for user : %s, msg: %s", user_id, str(exception))
            return HttpResponseBadRequest("Error while fetching user_balance : "+str(exception) )

@csrf_exempt 
@transaction.atomic
def add_balance(request):
    if request.method == "POST" :
        raw_data= request.body
        json_formatted_data = json.loads(raw_data)
        user_id = json_formatted_data['user_id']
        amount = json_formatted_data['amount']
        if amount > 0 and user_id is not None:
            wallet_service.add_balance(user_id, amount)
            return HttpResponse("Added Balance")
        else :
            logger.error("Invalid details")
            return HttpResponseBadRequest("Invalid user_id")
    else:
        logger.error("Unsupported method type for add balance %s", request.method)
        return HttpResponse(status=501, content = "Unsupported method") # should throw 501 unsupported method

@csrf_exempt
@transaction.atomic
def make_payment(request):
    if request.method == "POST": 
        raw_data = request.body
        json_formatted_data = json.loads(raw_data)
        from_user = json_formatted_data['from_user']
        to_user = json_formatted_data['to_user']
        amount = json_formatted_data['amount']
        return validate_and_transfer_money(from_user, to_user, amount)
    else:
        logger.error("Unsupported method type for make_payment %s", request.method)
        return HttpResponse(status=501, content = "Unsupported method")


@csrf_exempt
@transaction.atomic
def request_payment(request):
    if request.method == "POST" :
        raw_data= request.body
        json_formatted_data = json.loads(raw_data)
        requestee = json_formatted_data['requestee']
        requester = json_formatted_data['requester']
        amount = json_formatted_data['amount']
        return validate_and_transfer_money(requestee, requester, amount)
    else:
        logger.error("Unsupported method type for make_payment %s", request.method)
        return HttpResponse(status=501, content = "Unsupported method")


@csrf_exempt
def owns_money_from(request, user_id):
    if user_id is None:
        logger.error("Invalid user_id")
        return HttpResponseBadRequest("Invalid user_id")
    else:
        user_list = wallet_service.current_user_owns_from(user_id)
        logger.info("return list of users which current user owns_money_from :%s", user_id)
        return HttpResponse(status = 200, content = json.dumps(user_list, indent = 4))

@csrf_exempt
def owns_money_to(request, user_id):
    if user_id is None:
        logger.error("Invalid user_id")
        return HttpResponseBadRequest("Invalid user_id")
    else:
        user_list = wallet_service.current_user_owns_to(user_id)
        logger.info("return list of users which current user owns_money_to :%s", user_id)
        return HttpResponse(status = 200, content = json.dumps(user_list, indent = 4))





def validate_and_transfer_money(from_user, to_user, amount):
    if from_user is None or to_user is None or amount is None:
        logger.error("Invalid details from_user: %s to_user: %s  amount: %s", from_user, to_user, str(amount))
        return HttpResponseBadRequest("Invalid request values")
    elif type(amount) != float or amount < 0:
        logger.error("amount type is invalid: %s", str(amount))
        return HttpResponseBadRequest("Invalid amount value")
    else:
        try:
            wallet_service.transfer_money(from_user, to_user, amount)
            return HttpResponse(status = 200, content = "money transfered")
        except Exception as exception:
            logger.error("Error while transfering amount from_user : %s, to_user: %s, amount: %s, msg: %s", from_user, to_user, str(amount), str(exception))
            return HttpResponseBadRequest("Error while transfering amount from_user : "+from_user+", to_user: "+to_user+", amount: "+str(amount)+", msg: "+str(exception))
