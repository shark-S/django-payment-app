from paymentApp.models.wallet import Wallet
from paymentApp.models.transaction import Transaction
from paymentApp.models.aggregate_transaction import AggregateBalance
from django.utils import timezone
import uuid
import logging

logger = logging.getLogger(__name__)

started_status = "STARTED"
completed_status = "COMPLETED"

def get_balance(user_id):
    user_wallet = Wallet.objects.filter(user_id = user_id)

    if len(user_wallet) > 0 :
        return user_wallet[0].balance
    else:
        logger.error("user doen't exists %s", user_id)
        raise ValueError("Invalid user_id")


def add_balance(user_id, amount):
    user_wallet_query_result = Wallet.objects.filter(user_id = user_id)

    user_wallet = None

    if len(user_wallet_query_result) > 0 :
        user_wallet = user_wallet_query_result[0]
    

    if user_wallet is None:
        logger.info("createing new user: %s with balance: %s", user_id, amount)
        user_wallet = Wallet(user_id = user_id, balance = amount)
        user_wallet.save()

    else:
        logger.info("updating user: %s balance: %s", user_id, amount)
        total_amount = user_wallet.balance + amount
        user_wallet.balance = total_amount
        user_wallet.save()

def transfer_money(from_user, to_user, amount):
    from_user_model = Wallet.objects.filter(user_id = from_user)

    if len(from_user_model) ==0:
        logger.error("from_user %s doesn't exists", from_user)
        raise ValueError("%s doesn't exists", from_user)
    from_user_model = from_user_model[0]

    if from_user_model.balance < amount:
        logger.error("Insufficient balance in account of user %s", from_user)
        raise ValueError("Insufficient balance in account of user %s", from_user)

    to_user_model = Wallet.objects.filter(user_id = to_user)

    if len(to_user_model) ==0:
        logger.error("to_user %s doesn't exists", to_user)
        raise ValueError("%s doesn't exists", to_user)
    to_user_model = to_user_model[0]

    logger.info("transfering money from user %s, to user %s amount %s", from_user, to_user, str(amount))
    
    transaction_history = Transaction(id=uuid.uuid1(), from_user = from_user, to_user = to_user, amount = amount, status = started_status, created_at = timezone.now(), updated_at = timezone.now())
    transaction_history.save()

    from_user_model.balance = from_user_model.balance  - amount

    to_user_model.balance = to_user_model.balance + amount

    from_user_model.save()
    to_user_model.save()

    maintain_aggregate_balance(from_user, to_user, amount)

    transaction_history.status = completed_status
    Transaction.updated_at = timezone.now()
    transaction_history.save()

    logger.info("money transfered successfully")


def maintain_aggregate_balance(from_user, to_user, amount):
    lender_to_borrower_mapping = AggregateBalance.objects.filter(from_user=from_user, to_user = to_user)
    borrower_to_lender_mapping = AggregateBalance.objects.filter(to_user=from_user, from_user = to_user)


    if len(lender_to_borrower_mapping) > 0 and len(borrower_to_lender_mapping) > 0:
        logger.info("updating aggregated balance maaping for users %s %s", from_user, to_user)

        lender_to_borrower_mapping = lender_to_borrower_mapping[0]
        borrower_to_lender_mapping = borrower_to_lender_mapping[0]

        lender_to_borrower_mapping.amount = lender_to_borrower_mapping.amount + amount
        lender_to_borrower_mapping.updated_at = timezone.now()

        borrower_to_lender_mapping.amount = borrower_to_lender_mapping.amount - amount
        borrower_to_lender_mapping.updated_at = timezone.now()

        lender_to_borrower_mapping.save()
        borrower_to_lender_mapping.save()


    else:
        logger.info("creting new aggregated balance maaping for users %s %s", from_user, to_user)
        lender_to_borrower_mapping = AggregateBalance(id = uuid.uuid4(), from_user = from_user, to_user = to_user, amount = amount, created_at = timezone.now(), updated_at = timezone.now())
        lender_to_borrower_mapping.save()
        
        borrower_to_lender_mapping = AggregateBalance(id = uuid.uuid4(), from_user = to_user, to_user = from_user, amount = ( amount * -1), created_at = timezone.now(), updated_at = timezone.now())
        borrower_to_lender_mapping.save()



def current_user_owns_to(user_id):
    balance_sheet = AggregateBalance.objects.filter(from_user = user_id).filter(amount__lt=0)

    list_user = {}

    if len(balance_sheet) > 0:
        for user_mapping in balance_sheet:
            list_user[user_mapping.to_user] = (user_mapping.amount * -1)
    else:
        logger.info("Empty list of users user %s own to", user_id)

    return list_user

def current_user_owns_from(user_id):
    balance_sheet = AggregateBalance.objects.filter(from_user = user_id).filter(amount__gt=0)

    list_user = {}

    if len(balance_sheet) > 0:
        for user_mapping in balance_sheet:
            list_user[user_mapping.to_user] = user_mapping.amount
    else:
        logger.info("Empty list of users user %s own from", user_id)

    return list_user